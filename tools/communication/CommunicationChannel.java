package atelier3.tools.communication;

import java.io.IOException;


public interface CommunicationChannel  {
	
	public void openConnection() throws IOException;
	public void closeChannel() throws IOException;

	public Object readMessage();
	public void writeMessage(Object message);
}
