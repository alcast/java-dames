# java-dames
### auteur : Albane CAStillon

## Projet
Il s’agit de réaliser un jeu de dames. 
Les deux joueurs peuvent jouer sur le même ordinateur ou sur 2 ordinateurs différents (et distants), la communication des données entre les deux se faisant à travers des sockets.
Seules les pièces du joueur courant (blanc ou noir) peuvent être déplacées. Le joueur blanc commence.
Pour déplacer une pièce sur le damier, le joueur la sélectionne avec la souris (clic) puis clique sur la case de destination. Si le déplacement est légal (selon les règles du jeu de dames), la pièce apparait sur la case de destination, sinon, elle reste à sa position initiale. 
Le déplacement d’un pion est légal s’il est effectué en diagonale dans une case adjacente si la case est vide ou dans la case adjacente à celle d’une pièce à prendre (toujours en diagonale). Une dame peut se déplacer de plusieurs cases, en diagonale à condition qu’il y ait maximum une pièce à prendre sur le trajet.
Les pièces prises disparaissent du jeu.

L'atelier 3 consistait à faire une partie de jeu de dames tous seul(e). C'est à dire qu'une même personne joue les pions blancs et les pions noirs.

## Atelier 4
L'atelier 4 consiste à améliorer le jeu. J'ai décidé de faire évoluer l’architecture de manière à ce que chaque joueur soit sur un ordinateur différent (on commencera à simuler en mode local : « 127.0.0.1 »). 
Pour cela, de nouveaux controllers ont été créés : ClientController, ServerController et RequestHandler.
Ensuite des launchers ont été créés:  LauncherServer , LauncherWhiteClient et LauncherBlackClient. Ils sont créés  pour qu'il existe deux applications clientes (1 pour le joueur blanc et 1 pour le joueur noir).

## Finalité du projet 
Le projet n'est pas fini. Un aspect important n'est pas présent : l'affichage de la view pour chaque joueur. 
La communication entre le serveur et le client fonctionne. Chaque mouvement est envoyé au serveur et il s'affiche.

## Améliorations futures
* affichage des plateaux pour chaque joueur.
* permettre d'avoir une file d'attente pour pouvoir jouer en continu.
